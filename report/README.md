# {Red/Purple} Team Operation {number of operation}: {short description}

**CONFIDENTIAL INFORMATION - Internal use only - Do not share any of this information externally.**

[TOC]

## Executive Summary

### Key Takeaways

_This is a bulleted list covering the overview and outcomes below. It should be very short, and written so that someone can read only this list and have a general understanding of the entire operation._

### Overview

_This should be a short overview (no more than a few pagragraphs) that describes the type of attack emulated, the goals of the operation, and when it was conducted._

### Outcomes

_This should be a short summary of what was achieved in the operation, especially as it relates to the goals that were defined early on. This section can also include graphics or charts that support the summary._

## Operation Detail

### Attack Paths

_This section should include a visual representation of the attack path, like a flow chart. At GitLab, we will use [MITRE ATT&CK Flow](https://github.com/center-for-threat-informed-defense/attack-flow) to build the flowchart. This gives us a standard format that can be imported into other tools like ATT&CK Navigator and third-party threat intelligence platforms._

### Attack Narrative

_An attack narrative tells the story of the attack, in a plain-English fashion._

### Technique Detail

_This table should contain relevant techniques from the Red Team Operator Log, listed in chronological order. At GitLab, we use the previous [MITRE ATT&CK Flow](https://github.com/center-for-threat-informed-defense/attack-flow) output ([STIX](https://oasis-open.github.io/cti-documentation/stix/intro) JSON format) as an input for a script that generates the technique detail table below._

| Date/Time | ATT&CK Tactic / Technique | Source | Target | Description | Outcome |
| ----- | ----- | ----- | ----- | ----- | ----- |
| x | x | x | x | x | x |
| x | x | x | x | x | x |
| x | x | x | x | x | x |

### Indicators of Compromise

_This should include the same list of IoCs documented in the adversary profile, including any new IoCs that were created during the operation._

#### Host-based

Below are indicators at the individual host level, typically for tooling used in the operation.

| Malicious Tool Name | Hostname       | File Path        | Hash     | Brief Description                              | Additional Info                                                         |
| ------------------- | -------------- | ---------------- | -------- | ---------------------------------------------- | ----------------------------------------------------------------------- |
| Poseidon C2 Agent   | testhost.local | /tmp/not_malware | (SHA256) | C2 agent used for initial and sustained access | Run as `PATH=$PATH:/tmp not_malware &`, to avoid `/tmp` in process list |
| x                   | x              | x                | x        | x                                              | x                                                                       |

#### Network-Based

Below are indicators at the network level, to include DNS names, IP addresses, specific URLs, User Agents, Email Indicators, etc.

| Indicator    | Indicator Type | Brief Description               |
| ------------ | -------------- | ------------------------------- |
| c2domain.com | Domain name    | Domain used for C2 comms (HTTP) |
| x            | x              | x                               |

#### Application-Based

Below are indicators of application-specific activity, such as malicious accounts, compromised target accounts, etc.

| Application   | Indicator              | Brief Description                                                                    |
| ------------- | ---------------------- | ------------------------------------------------------------------------------------ |
| Target WebApp | username: redteam-user | Free user account created by the red team for testing. Used to perform (x) activity. |
| x             | x                      | x                                                                                    |

## Recommendations

_A list of specific recommendations that came from the observations. Each recommendation should link to an issue or merge request that is assigned to an appropriate DRI._

**CONFIDENTIAL INFORMATION - Internal use only - Do not share any of this information externally.**

/label ~"Red Team Operation::Report"
/confidential
