# Red Team Recommendation

## Risk and Impact

> _Describe the specific risk, how it was discovered, and its potential impact. Provide evidence._

## Recommendation

> _Give a specific and actionable recommendation to address the risk. Ensure the recommendation is achievable with a clear, measurable objective that demonstrates success. If there are multiple recommendations, create them in separate issues._

## Additional Resources

> _Add links to any related Red Team reports, incidents, ATT&CK documentation, etc._
