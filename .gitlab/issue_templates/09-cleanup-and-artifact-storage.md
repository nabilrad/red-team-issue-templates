# Stage Nine: Cleanup and Artifact Storage

## Stage Summary

This stage is meant to track the cleanup process from any operation.  Typically, during the process of executing an operation, testing and supporting infrastructure is created and needs to be destroyed once the operation is over to save cost and reduce attack surface going forward.  Repeatable infrastructure, if needed, can be created in our [red-team-infrastructure](https://gitlab.com/gitlab-com/gl-security/threatmanagement/redteam/redteam-internal/red-team-infrastructure) project, and therefore easily torn down using terraform.

As you create supporting technical infrastructure and code to support an operation, add a checkbox in the "to-do" section as reminder to clean things up following the operation.  Be specific about the location of the items and when they should be destroyed.  If semi-permanent storage of operation artifacts is needed, this is typically done with a GCP storage bucket in the red team production project with a [1 year delete lifecycle](https://cloud.google.com/storage/docs/managing-lifecycles#set).

## To-Do

### Examples

- [ ] Delete supporting infrastructure in x project using y terraform project located [here](https://gitlab.com).
- [ ] Store operation artifacts in GCP storage bucket with an appropriate lifecycle.

/confidential
