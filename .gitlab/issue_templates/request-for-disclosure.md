# Red Team Disclosure Request

**IMPORTANT**: This issue template is for use by managers and above only. Please ensure that you have sufficient evidence to support your belief that an observed activity is a Red Team operation. Do not delay in following normal reporting procedures for any security incidents.

## Overview

If you or your team believe you have uncovered a [stealth operation](https://handbook.gitlab.com/handbook/security/threat-management/red-team/#stealth-operations), you can submit an issue using this template. This does not replace the standard security reporting process, and is only necessary when you need to prioritize resources for maintaining production and protecting against real threats.

The Red Team will reply only with a confirmation that we received the issue. If the evidence you submit is related to an ongoing operation, we will discuss next steps with the operation's "trusted participants".

You can read more about this [here](https://handbook.gitlab.com/handbook/security/threat-management/red-team/#is-this-the-red-team).

## Request Details

### Summary

> _Please summarize the incident and why you are opening this issue_

### Incident Details

> _You can fill in the details below or provide a link to a confidential issue that contains answers to each bullet_

- Date and time of observation:
- Systems or assets involved:
- Indicators of Compromise:
- Specific details which makes you think this is a Red Team operation:

/label ~"Request For Disclosure"
/confidential
cc: @gitlab-red-team
