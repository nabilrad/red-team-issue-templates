# Stage Two: Logistics

## Stage Summary

This stage is used to decide upon logistics like operational goals, roles, and responsibilities. Having this documented early on is helpful to ensure things go according to plan.

## To-Do

- [ ] Fill in the "Roles and Responsibilities" section below.
- [ ] Fill in the "Operational Goals" section below.
- [ ] Fill in the "Deconfliction" section below.
- [ ] Establish a communication channel for trusted participants, like a private Slack room.
- [ ] Share the "Operational Goals" and "Deconfliction Process" with all trusted participants. Ask for feedback and make changes if needed.
- [ ] Document approval from owners and executive sponsors in the issue comments below.

-----

## Roles & Responsibilities

### Team Leads

While everyone on the team can participate equally, we should define who is ultimately responsible for ensuring actions are completed in a timely fashion. The lead can be a manager or an IC, as long as they can commit to being the coordinator for their team's responsibilities.

- Red Team Lead:
- Blue Team Lead:

### Group Owners

A single owner should be defined for each group that the operation involves. They are responsible for signing off on the goals defined in this issue, as well as the techniques that will be finalized in the tabletop exercise.

- Red Team Owner:
- Blue Team Owner:
- Infrastructure Team Owner:

### Additional Roles

The following roles may come in to play when performing unannounced Red Team operations or other advanced operations that span more than the security organization. They may not be necessary for Purple Team operations done in the open.

- Trusted Participants:
  - Group XXX:
  - Group YYY:
  - Group ZZZ:
- Executive Sponsor:
- Legal Owner:
- HR Owner:
- Communications Owner:

## Operational Goals

> _Provide a short summary of what we are doing and why we are doing it. Include the scope of the operation and a bulleted list of specific and measurable goals. Depending on the intent of the operation, those goals may be attacker objectives (gain access to x), business objectives (improve the capabilities of x), or both._
>
> _Some operations may have a specific goal to test the processes and procedures of a response. But what if the Red Team emulates a complete attack and does not trigger any detection/response activities? Make sure to detail whether or not techniques will be escalated to ensure a response occurs._
>
> _This summary should provide a concise but complete summary of the operation, as it is shared with the trusted participants and may be the first thing they read._

## Deconfliction Process

For stealth operations, answer the following questions:

- How will the trusted participants contact the Red Team to ask if an activity is theirs?
- How will the Red Team respond if someone other than a trusted participant asks if an activity is theirs?
  - At GitLab, we provide this answer in our handbook: [Is this the Red Team?](https://about.gitlab.com/handbook/security/threat-management/red-team/#is-this-the-red-team)
- How far will detection and response activities be allowed to go before the attack is disclosed as being a Red Team operation?
- What steps will the Red Team and the trusted participants take to allow an operation to continue in stealth after some detection and response activities occur?
- If the attack techniques are disclosed as a Red Team operation, what happens next? Options may be to switch to a Purple Team operation, to conduct "virtual containment" of systems exploited thus far and to start again, or to halt the operation completely.

/confidential

## Message Templates

This section contains templates for messages we send in Slack. There is nothing to complete in this section, it is just a resource to use as needed.

### Reaching out to potential trusted participants

> Hi _NAME_!
>
> I'm an engineer on the [Red Team](https://handbook.gitlab.com/handbook/security/threat-management/red-team/). We conduct security exercises that emulate real-world threats.
>
> We're current planning for an upcoming [stealth operation](https://handbook.gitlab.com/handbook/security/threat-management/red-team/#stealth-operations). These are exercises where only a small group of people, who we call "trusted participants", are aware of the details.
>
> We've identified you as someone who could be particularly helpful in an upcoming operation. If you're interested, we'd invite you to a private Slack channel with other trusted participants to discuss the details. We'd ask that you keep all the information shared there confidential to that group.
>
> The operation will be fully approved by GitLab security senior leadership, and it shouldn't take up too much of your time. Once the operation is complete, the details will be disclosed internally to all team members.
>
> There's no pressure to participate, but we would love to have you on the team. Let me know what you think, or if you have any questions before deciding.
>
> Thanks!

### Announcing the operation to trusted participants in the private Slack channel

> Hi everyone!
>
> Welcome to the "trusted participants" channel for the next Red Team [stealth operation](https://handbook.gitlab.com/handbook/security/threat-management/red-team/#stealth-operations).
>
> You've also been added to a new group on GitLab.com. Inside that group is [this epic](/#link-epic-here) where the bulk of the operation planning will occur. That group, the epic, and everything it contains is private. It will be made available internally at GitLab once the operation is complete.
>
> We ask that you please keep all details of this operation private. This helps us to provide a realistic opportunity for GitLab to practice detecting and responding to real-life attacks.
>
> You can use this channel to raise any questions about the operation and to check if any activities observed outside of this channel are related.
>
> We are still finalizing the operation details using our ["logistics" template](https://gitlab.com/gitlab-com/gl-security/threatmanagement/redteam/redteam-public/red-team-issue-templates/-/blob/main/.gitlab/issue_templates/02-logistics.md?ref_type=heads). This will include things like:
>
> - Operational goals, including a brief summary of the attack being emulated
> - What we will do if/when we are detected (aka "deconfliction" process)
>
> We will check back when that is complete and share the details with you all.
>
> Thanks so much for participating!

### Sharing the operation logistics with the trusted participants

> Hi everyone!
>
> We've completed [the logistics phase of our operation](/#provide-link). This includes defining roles, goals, and a deconfliction process.
>
> We invite all trusted participants to please read the linked issue and provide any feedback there.
>
> Additionally, we ask that the operation sponsors please comment in the issue with your approval or any changes you'd like us to make first. This includes _(cc slack names here)_.
>
> Thanks everyone!
