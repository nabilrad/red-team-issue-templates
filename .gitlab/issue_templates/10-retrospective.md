# Stage Ten: Retrospective

## Stage Summary

This stage is meant to gather feedback on the operation - what went well and what could be improved in the future. Feedback is welcome from anyone - whether they were directly involved in the operation or not.

A free-form discussion can be conducted in the issue comments below, but the highlights should be extracted and placed into the issue description.

## To-Do

- [ ] Share a link to this issue with all operation participants and stakeholders
- [ ] After 1 week, extract the highlights from the issue comments and put them into the sections below

-----

## What went well

- xxx
- xxx

## What could be improved

- xxx
- xxx

/confidential
