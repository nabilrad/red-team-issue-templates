# Red Team Opportunistic Attack

## Goals

> _Provide a short summary of what you are doing and why you are doing it. Include a bulleted list of specific and measurable goals. This can include attacker objectives (gain access to x), proving a hypothesis (a specific service or behavior is exploitable), or other goals to reduce risk at GitLab._
>
> _Try to aim for goals that have a broad impact. For example, finding one vulnerable public resource is good. Finding out how it got there, and providing a recommendation to prevent it in the future is better._

## Scope and Timeframe

> _Document the systems in scope for the attack and how long you plan to spend on it._

## Tasks

> _Outline a high-level plan for conducting your attacks._

/confidential
/label ~"RTWork::OpportunisticAttack"
