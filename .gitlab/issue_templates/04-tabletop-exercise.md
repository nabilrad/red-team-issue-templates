# Stage Four: Tabletop Exercise

## Stage Summary

This stage is an asynchronous version of a tabletop exercise. The goal is to set a baseline of expectations - what do we think will be the result of the attack based on our current detection and response capabilities?

This stage doesn't require every technique to have a detection mechanism already implemented. One of the reasons for emulating attacks in the first place is to identify gaps in detection capabilities and organizational processes. Performing an operation where gaps exist can help us understand whether or not there is a real risk and propose improvements.

We can also discuss changing, removing, or adding techniques during this stage. For Purple Team operations, we should ensure that at least one technique will trigger an alert. Without that, there would be no opportunity to practice responding and investigating. For unannounced Red Team operations, however, the goal may actually be to completely avoid detection.

## To-Do

- [ ] Red Team: Fill in the "Scenario(s)" section below using the previously created adversary profile.
- [ ] Red Team: Fill in the "Tactic" and "Technique" columns inside the "Asynchronous Tabletop" chart below using the previously created adversary profile.
- [ ] Red Team / Blue Team: Fill in the "Expected IoCs" column below using the previously created adversary profile.
- [ ] Blue Team: Fill in the "Expected Detection Details" column below.
- [ ] Blue Team: If a relevant run-book exists, link it in the column below.
- [ ] Blue Team: If any work is required to ensure the expected detections will function, open a new issue and mention it in the comments below. For example, allow-listing a test account to trigger an existing production alert.
- [ ] Red Team: Propose any injects (modifications/updates) to the tabletop scenario(s) at hand and allow participants to adjust their responses.
- [ ] Red Team: Create a separate issue detailing the next steps resulting from the tabletop exercise.  Link the newly created issue to the tabletop issue.

-----

## Scenario(s)

Add details on the scenario(s) relevant to the asynchronous tabletop.

## Asynchronous Tabletop

| Tactic | Technique | Expected IoCs | Expected Detection Details | Run-Book |
| ------ | ------ | ------ |------|------|
| x | x | x | x | x |
| x | x | x | x | x |
| x | x | x | x | x |
| x | x | x | x | x |
| x | x | x | x | x |

Note: the "Expected Detection Details" column should include the specific tooling involved.

/confidential
