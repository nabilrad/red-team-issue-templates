# OKR Planning

This issue is to collaborate on the planning of [OKRs](https://about.gitlab.com/company/okrs/) before submitting them to the organization. Please edit the issue description with relevant details. Less formal conversation on the topic can be conducted in the issue comments.

We will being planning for OKRs in the middle of each quarter, with the goal of being ready to input them two weeks before the new quarter begins.

The OKR should be stated in the form of an objective with associated key results. More info can be found in "[Fundamentals of Impactful OKRs](https://about.gitlab.com/company/okrs/#fundamentals-of-impactful-okrs)"

## Overview

- Quarter: [FYXXQXX]
- Date Range: [YYYY-MM-DD - YYYY-MM-DD]
- Start planning: [YYYY-MM-DD]
- Complete planning by: [YYYY-MM-DD]

## OKRs

- Objective 1
  - Key result
  - Key results
  - Key result
- Objective 2
  - Key result
  - Key result
  - Key result

## Resources

- [OKR Process at GitLab](https://about.gitlab.com/company/okrs/#okr-process-at-gitlab)

/due [DATE HERE]
/label "Red Team::OKR Planning"
