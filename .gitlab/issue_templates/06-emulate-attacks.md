# Stage Six: Emulate Attacks

## Stage Summary

This is the stage where the Tactics, Techniques, and Procedures (TTPs) defined earlier will actually be executed. This might be as simple as running a CI pipeline with all techniques automated, or much more involved in the case of a manual operation.

This stage may involve synchronous meetings including both Red Team and Blue Team members. It may also be conducted at the same time as the next stage (Validate Detection & Response).

## To-Do

- [ ] Schedule synchronous meetings when appropriate, filling in the "Synchronous Meetings" section below.
- [ ] Execute the TTPs as agreed upon in the previous stages.
- [ ] Complete the Red Team operator logs. This can be done in the table below, or as individual comments in this issue.
- [ ] Ensure all log files are available for review.
- [ ] Conduct cleanup activities as required.
- [ ] Fill in the "Attack Narrative" section in the final report.
- [ ] Fill in the "Significant Observations" in the final report.
- [ ] Fill in the " AdditionalFindings" section in the final report.

-----

## Synchronous Meetings

> _This section should be used to plan meeting times and participants_

| Date/Time | Participants | Completed? |
| ----- | ----- | ----- |
| x | x | x |
| x | x | x |
| x | x | x |
| x | x | x |

## Red Team Operator Logs

> _This section should be filled out during the operation. Alternatively, the issue comments can be used as a running log of attack techniques._

| Date/Time | Operator Name | Source | Target | Description | IoC | Evidence |
| ----- | ----- | ----- | ----- | ----- | ----- | ----- |
| x | x | x | x | x | x | x |
| x | x | x | x | x | x | x |
| x | x | x | x | x | x | x |
| x | x | x | x | x | x | x |

## Identified Cleanup Areas

> _Use this section to log cleanup tasks identified during the operation_

- ...
- ...
- ...

/confidential
