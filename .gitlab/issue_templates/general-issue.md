# {Issue Title}

## Overview (be Specific)

> _Add in an overview of what you want to complete in this project or task. Be specific with what needs to be completed, fixed, or improved_

## Objectives (be Measurable and Achievable)

> _Add in what you plan to complete. If there are multiple phases, break it down into a list of milestones. Be realistic and don't forget that iteration is expected! Go with the boring solution, and don't try to fix it all during the first iteration._

| Task | Due date | DRI | Status | % Overall Completion |
|------|------|------|------|------|
| ... | ... | ... | ... | xx% |
| ... | ... | ... | ... | xx% |
| ... | ... | ... | ... | xx% |

## Outcomes (be Relevant)

> _What are the outcomes that will be delivered when this project or task is completed? How does this align with broader values and goals?_

## Target Completion Date (set a Time-bound target)

> _When will the project or task be completed?_
