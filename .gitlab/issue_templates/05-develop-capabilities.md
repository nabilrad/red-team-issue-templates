# Stage Five: Develop Capabilities

## Stage Summary

This stage is focused on developing the offensive capabilities required to execute each technique outlined in the adversary profile and reviewed in the tabletop exercise. Automation should be used whenever possible.

This may require the setup of a Command & Control (C2) environment, which would also be outlined in the previously-completed adversary profile.

## To-Do

- [ ] Choose an automation platform (i.e. GitLab CI or Caldera):
- [ ] Create a project to store all code/scripts:
- [ ] R&D for specific tech/subject (when required)
- [ ] Deploy C2 infrastructure (when required)
- [ ] Write and test all automation code for each technique (see next section)

-----

## Breakdown of Required TTP Development

The following table can be used to track each individual technique that requires scripting/coding, etc.

| Technique | Link to MR | Completed? |
| ----- | ----- | ----- |
| x | x | x |
| x | x | x |
| x | x | x |
| x | x | x |

/confidential
