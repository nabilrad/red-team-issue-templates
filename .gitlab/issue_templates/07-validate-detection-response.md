# Stage Seven: Validate Detection & Response

## Stage Summary

This is the stage where we compare the expected outcomes from our tabletop phase to the actual outcomes from emulating the attack. The work here is owned by our Blue Team, but it may be done synchronously with the Red Team at the same time as the previous stage (Emulate Attacks).

## To-Do

- [ ] Schedule synchronous meetings when appropriate, filling in the "Synchronous Meetings" section below.
- [ ] Work through each technique inside Vectr, filling in the following:
  - Outcome: Blocked, Detected, Not Detected (logged / not logged).
  - Detecting Blue Tools: The specific tools that contributed to the outcome.
  - Outcome notes: Additional relevant information.
- [ ] Fill in the "Significant Observations" section in the final report.

-----

## Synchronous Meetings

> _This section should be used to plan meeting times and participants_

| Date/Time | Participants | Completed? |
| ----- | ----- | ----- |
| x | x | x |
| x | x | x |
| x | x | x |
| x | x | x |

/confidential
