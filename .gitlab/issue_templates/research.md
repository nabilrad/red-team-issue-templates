# Red Team Research

## Goals

> _Provide a short summary of what you are doing and why you are doing it. Include a bulleted list of specific and measurable goals. This should include what the output of your research will be - such as a blog, conference talk, vulnerability disclosure, etc._

## Timeframe

> _Document the time you plan to spend on your research._

## Tasks

> _Outline a high-level plan for conducting your research._

/confidential
/label ~"RTWork::Research"
