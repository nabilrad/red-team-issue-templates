# DRAFT: {number of operation} Tabletop Report: {short description}

## Overview

> _This should be a short summary, one to two paragraphs, outlining the general goals of the tabletop exercise and the included scenarios._

## Evidence and Research

> _Provide links to any evidence and research that led to the tabletop exercise._

## Threat Model

> _Provide a graphic and either a link to a documented threat model, or a direct inclusion of the threat model if possible._

## Relevant Scenarios

> _What attack scenario(s) will be covered during the tabletop?_

* ...
* ...

## Roles

> _Which internal or external roles are relevant to the discussion?_

* SIRT
* SRE
* Infrastructure Engineering

## Participants

* Facilitator: _Name_
* Players: _List of names and roles involved_
  * ...
  * ...

## Important Links and Documentation

> _Provide a link to the Google Doc used to facilitate the discussion along with important research, notes, etc._

## Key Findings

> _What were the most critically relevant findings as a result of the exercise._

## Recommendations

> _What recommendations can red team make to mitigate the key findings?_

/label ~"Red Team Operation::Report"
/confidential
