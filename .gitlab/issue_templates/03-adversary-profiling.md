# Stage Three: Adversary Profiling

## Stage Summary

This stage is where the bulk of the planning work is done. Based on the rough concept from the ideation phase, we will build out a detailed profile of the adversary. This will help bring realism into the exercise, ensuring we are preparing to defend against attacks as they are most likely to happen.

As much as possible, the sections below should come from realistic sources (Threat Intelligence, documented APT behavior, etc.).

Everyone is invited to contribute here, as each team will have unique insights to add.

## To-Do

- [ ] Complete the "Adversary Overview" section below
- [ ] Complete the "TTPs" section below
- [ ] Complete the "IOCs" section below
- [ ] Complete the "C2" section below (if relevant)

-----

## Adversary Profile

### Adversary Overview

- Threat Scenario: <!--the narrative, or the story of the attack-->
- Intent: <!--what is the attacker trying to do, and why? do they have something specific they are trying to access?-->
- Capability: <!--what is the skill level and resources available to the attacker?-->
- Opportunity: <!--how much time and knowledge of the target does the attacker have? Is there a vulnerability available for them to exploit?-->

Choose one of the following scenario models:

- [ ] Assumed breach. Document the "starting point" here:
- [ ] Full engagement. This is a complete end-to-end attack including OSINT and initial access. Note that this model will require significantly more time and effort.

### Tactics, Techniques, Procedures (TTPs)

> _This should be a detailed breakdown of how this adversary operates. When possible, try to map to MITRE ATT&CK._
>
> _While we should work to completely define the TTPs here, it is possible they may evolve more during the tabletop stage. For example, some may be deemed unnecessary as there is no way possible to ever detect/respond to them, etc._

### Indicators of Compromise

> _List the indicators of compromise (IoCs) the Red Team expects to generate. These should be very specific data points that can be used for threat hunting and incident response._

- Network-based IoCs (DNS names, IP address, URLs, User Agents, Email Indicators, etc.):
  - ...
  - ...
- Host-based IoCs (File names, file hashes, file sizes, process names, etc.):
  - ...
  - ...

### Command and Control (C2) Infrastructure

> _Where relevant, document the C2 profile._

- C2 Platform: <!--i.e. Caldera, Cobalt Strike, etc-->
- Agent outbound ports:
- Agent outbound protocol:
- C2/redirector hostnames:
- Agent system changes: <!--any artifacts left on host-->

/confidential
