# Stage One: Brainstorming

## Stage Summary

This is a fairly free-form stage, where we propose ideas that can be built into a full-scale operation. These will generally come from one of the following sources:

- Cyber Threat Intelligence (CTI) - we have gained some level of understanding about an active industry threat.
- New systems or functionality - we've been informed of new or upcoming systems and asked to emulate an attacker targeting that system.
- New detection capabilities - the Blue Team has implemented a set of new detection rules, etc. and wants to see how they work in practice and how they might be bypassed.
- Some other "hot topic" hypothetical - possibly leadership is concerned about a "what if" scenario and wants to emulate an attack. For example, "what if an endpoint was compromised?" or "what if a database was leaked?".

It is important to include a note on why we would do this exercise. What would our organization gain from doing this?

Specifically for GitLab, here are some sources of inspiration:

- [GitLab's StORM report](https://about.gitlab.com/handbook/security/security-assurance/security-risk/storm-program), which highlights security operational risks in support of GitLab's strategy.
- GitLab's "[Mitigating Concerns](https://internal.gitlab.com/handbook/leadership/mitigating-concerns/)" internal handbook page.
- GitLab SIRT Team [Monthly Threat Intel](https://internal.gitlab.com/handbook/security/security_operations/sirt/operations/threat_intelligence/threat_intelligence_report/) report
- GitLab's Red Team ThreatQ dashboards:
  - [Intel471 Reports Dashboard](https://gitlab.threatq.online/dashboard/10)
  - [Tech Interests Dashboard](https://gitlab.threatq.online/dashboard/11)
- Relevant public CTI reports, such as:
  - Threat Horizons report from [Google Cybersecurity Action Team](https://cloud.google.com/security/gcat)
  - Annual "Cloud Risk" report, "Global Threat Report", and more from [CrowdStrike](https://www.crowdstrike.com/resources/reports/).
  - Annual "M-Trends", "Defender's Advantage", and more from [Mandiant](https://www.mandiant.com/resources/reports).
  - CISA's [Cybersecurity Alerts and Advisories](https://www.cisa.gov/news-events/cybersecurity-advisories).

## To-Do

- [ ] Outline the idea in plain English below.
- [ ] Open the idea to discussion between relevant teams by `@`'ing them in this issue and sharing it in Slack.
- [ ] After reaching consensus on the idea being valid, close this issue and proceed with logistics.

-----

## Idea Overview

> _Use this section to outline the idea and the background. Make sure to include an explanation of "Why" - what would our organization gain from this type of exercise?_

/confidential
