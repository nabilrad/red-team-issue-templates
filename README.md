# Red Team Issue Templates

This repository contains file and issue templates used by [GitLab's Red Team](https://about.gitlab.com/handbook/security/threat-management/red-team/).

We use these templates when planning, tracking, and reporting on a Red Team operation. Working asynchronously across time zones, we find it very helpful to clearly document each stage of an operation using GitLab issues which we organize inside GitLab epics.

We have two ways of consuming this project, both in the form of private forks:

1. As a general purpose issue tracker, using the included issue templates.
2. As self-contained project for tracking individual stealth operations. This allows us to easily move the project between groups once the operation is disclosed and the report is being written.

The following resources describe how and why we use the templates:

- [What the Red Team does](https://about.gitlab.com/handbook/security/threat-management/red-team/#what-the-red-team-does)
- [Purple Teaming at GitLab](https://about.gitlab.com/handbook/security/threat-management/red-team/purple-teaming/)
- [How we run Red Team operations remotely](https://about.gitlab.com/blog/2022/05/11/how-we-run-red-team-operations-remotely/)

You can read more about using issue templates [here](https://docs.gitlab.com/ee/user/project/description_templates.html#use-the-templates).

## Contributing

Feel free to open an issue or a merge request. Because we use these templates ourselves, we may choose not to accept changes that don't quite fit our own needs - even if they make sense elsewhere.

Open an issue first to discuss if you are unsure whether to pursue changes that may not be accepted.

## Special Thanks

We have been inspired by the following resources and highly recommend taking a look:

- [Red Team Development and Operations](https://redteam.guide/): An excellent book by Joe Vest and James Tubberville.
- [Purple Teaming Execution Framework](https://github.com/scythe-io/purple-team-exercise-framework): Another great resource, this one by Scythe.
